const Discord = require('discord.js')
const client = new Discord.Client()
const configSecret = require('./config.secret.json')
const DBL = require('dblapi.js')
const dbl = new DBL(configSecret.apiKeyBL, client)

exports.client = client
exports.dbl = dbl
