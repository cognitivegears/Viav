const { validations } = require('../commander')

validations.push({
  run: ({ message, lang, guildConfig }) => {
    if (message.attachments.size > 0) {
      if (guildConfig.noImageChannels.includes(message.channel.id)) {
        const author = message.author
        let send = channel => {
          channel.send(
            `Please, don't send images in **#${message.channel.name}**.`
          )
        }
        if (author.dmChannel) {
          send(author.dmChannel)
        } else {
          author.createDM().then(send)
        }
        message.delete()
        return false
      }
    }
  }
})
