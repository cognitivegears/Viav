const { validations } = require('../commander')
const { sendDM } = require('../messager')

validations.push({
  run: ({ message, lang, guildConfig }) => {
    if (
      guildConfig.blockInvites &&
      message.content.includes('discord.gg') &&
      !(guildConfig.ops && guildConfig.ops.includes(message.member.id)) &&
      message.member !== message.guild.owner
    ) {
      sendDM(
        message.author,
        `Please, don't send invites in **${message.guild.name}**.`
      )
      message.delete()
      return false
    }
  }
})
