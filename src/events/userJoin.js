const { client } = require('../discordClient')
const configSecret = require('../config.secret.json')
const { getConfig, mergeConfig } = require('../util/guildFunctions')

client.on('guildMemberAdd', guildMember => {
  if (guildMember.user.bot && guildMember.id in configSecret.children) {
    mergeConfig(guildMember.guild.id, { musicBots: [guildMember.id] })
  }
})
