const { client } = require('../discordClient')
const request = require('request')
const getYoutubeID = require('get-youtube-id')
const fetchVideoInfo = require('youtube-info')
const config = require('../config.json')
const configSecret = require('../config.secret.json')
const { commands } = require('../commander')
const { createPlugin } = require('../plugin')
const { sendMessage } = require('../messager')
const { getLang } = require('../util/lang')
const { VoiceChannel } = require('discord.js')
const { getConfig } = require('../util/guildFunctions')
const fetch = require('node-fetch')
const strings = require('../strings.json')

const dispatchers = new Map()
const queues = new Map()
const voiceChannelBots = new Map()

const plugin = createPlugin('Music')

function childBotRun(command, options, voiceChannel, bot = null) {
  return new Promise((resolve, reject) => {
    if (!voiceChannelBots.has(voiceChannel.id)) {
      reject()
      return
    }
    bot = bot || voiceChannelBots.get(voiceChannel.id)
    fetch(`${bot.ip}/${command}`, {
      method: 'POST',
      body: JSON.stringify(options),
      headers: { 'Content-Type': 'application/json' }
    })
      .then(response => {
        return response.json()
      }, console.error)
      .then(data => {
        resolve(data)
      })
      .catch(console.error)
  })
}

function getIsBotAvailable(bot, voiceChannel) {
  return new Promise((resolve, reject) => {
    fetch(`${bot.ip}/available`, {
      method: 'POST',
      body: JSON.stringify({
        guild: { id: voiceChannel.guild.id },
        voiceChannel: { id: voiceChannel.id }
      }),
      headers: { 'Content-Type': 'application/json' }
    })
      .then(response => {
        console.log(response)
        return response.json()
      }, console.error)
      .then(data => {
        resolve(data.available)
      })
      .catch(console.error)
  })
}

function getAvailableBots(guildConfig, voiceChannel) {
  return new Promise((resolve, reject) => {
    function getAllBots() {
      const availableBots = []
      let botsChecked = 0
      console.log(guildConfig.musicBots)
      for (var i in guildConfig.musicBots) {
        const musicBotId = guildConfig.musicBots[i]
        const bot = configSecret.children[musicBotId]
        if (bot.enabled) {
          fetch(`${bot.ip}/available`, {
            method: 'POST',
            body: JSON.stringify({
              guild: { id: voiceChannel.guild.id },
              voiceChannel: { id: voiceChannel.id }
            }),
            headers: { 'Content-Type': 'application/json' }
          })
            .then(response => {
              return response.json()
            }, console.error)
            .then(data => {
              console.log(data)
              if (data.available) {
                availableBots.push(bot)
              }
              botsChecked++
              console.log('response')
              console.log(`${botsChecked} / ${guildConfig.musicBots.length}`)
              if (botsChecked === guildConfig.musicBots.length) {
                resolve(availableBots)
              }
            })
            .catch(() => {
              botsChecked++
              console.log('catch')
              console.log(`${botsChecked} / ${guildConfig.musicBots.length}`)
              if (botsChecked === guildConfig.musicBots.length) {
                console.log(availableBots)
                resolve(availableBots)
              }
            })
        } else {
          botsChecked++
          console.log(bot)
          console.log(`${botsChecked} / ${guildConfig.musicBots.length}`)
          if (botsChecked === guildConfig.musicBots.length) {
            console.log(availableBots)
            resolve(availableBots)
          }
        }
      }
    }
    if (voiceChannelBots.has(voiceChannel.id)) {
      const bot = voiceChannelBots.get(voiceChannel.id)
      getIsBotAvailable(bot, voiceChannel).then(isAvailable => {
        if (isAvailable) {
          resolve([bot])
        } else {
          getAllBots()
        }
      })
    } else {
      getAllBots()
    }
  })
}

function searchSong(searchQuery) {
  return new Promise((resolve, reject) => {
    console.log(searchQuery)
    const requestQuery =
      'https://www.googleapis.com/youtube/v3/search?part=id&type=video&q=' +
      encodeURIComponent(searchQuery) +
      '&key=' +
      configSecret.apiKeyYT
    console.log(requestQuery)
    request(requestQuery, function(error, response, body) {
      const json = JSON.parse(body)
      if (json.items[0]) {
        console.log(json.items[0].id.videoId)
        resolve(json.items[0].id.videoId)
      } else {
        reject('video not found')
      }
    })
  })
}

plugin
  .command('play', {
    description: 'Joins the users channel and plays a given song',
    alias: ['p'],
    run: ({params, message, callback, lang}) => {
      const guildConfig = getConfig(message.guild.id)
      // Get song serch
      const searchQuery = params.join(' ')
      // Echo: Searching for (song)
      if (!searchQuery.startsWith('http'))
        sendMessage(message.channel, `${lang.searching_for} **${searchQuery}**`)
      // Find song on YouTube
      searchSong(searchQuery).then(id => {
        // Get song name
        fetchVideoInfo(id, function(err, videoInfo) {
          if (err) {
            console.error(JSON.stringify(err))
            return
          }
          getAvailableBots(guildConfig, message.member.voiceChannel).then(
            availableBots => {
              if (availableBots.length === 0) {
                callback(
                  'No available music bots. ' +
                    'You can add another one by clicking ' +
                    '**[here](https://viav.app/add)**'
                )
                return
              }
              let bot = availableBots[0]
              voiceChannelBots.set(message.member.voiceChannel.id, bot)
              fetch(`${bot.ip}/play`, {
                method: 'POST',
                body: JSON.stringify({
                  guild: { id: message.guild.id },
                  voiceChannel: { id: message.member.voiceChannel.id },
                  textChannel: { id: message.channel.id },
                  stayInVoice: !!guildConfig.stayInVoice,
                  song: {
                    id,
                    title: videoInfo.title,
                    live: videoInfo.duration === 0
                  }
                }),
                headers: { 'Content-Type': 'application/json' }
              })
                .then(response => {
                  return response.json()
                }, console.error)
                .then(data => {
                  callback(`Queued **${videoInfo.title}**`)
                })
                .catch(console.error)
            }
          )
        })
      })
    }
  })
  .command('radio', {
    description: 'Plays a radio station. Use `stations` for a list of stations.',
    alias: ['r'],
    run: ({params, message, callback, lang}) => {
      const radioName = params[0] || Object.keys(config.stations)[0]
      if (radioName in config.stations) {
        commands.play.run([config.stations[radioName]], message, callback)
      } else {
        callback(`**${radioName}** ${lang.is_not_a_station}.`)
        commands.stations.run([], message, callback)
      }
    }
  })
  .command('stations', {
    description: 'Lists available radio stations.',
    alias: ['st'],
    run: ({params, message, callback}) => {
      const guildConfig = getConfig(message.guild.id)
      callback(
        '**Radio Stations:**\n' +
          Object.keys(config.stations).join('\n') +
          `\n\n\`${guildConfig.prefix}radio [station]\` to play a station.`
      )
    }
  })
  .command('skip', {
    description: 'Skips current song',
    alias: ['sk'],
    run: ({params, message, callback}) => {
      sendMessage(message.channel, 'Skipping song', 5)
      childBotRun(
        'skip',
        {
          guild: { id: message.guild.id },
          voiceChannel: { id: message.member.voiceChannel.id }
        },
        message.member.voiceChannel
      )
        .then(data => {
          callback('Skipped')
        })
        .catch(() => {
          callback("Can't skip a song I'm not playing")
        })
    }
  })
  .command('queue', {
    alias: ['que', 'q'],
    run: ({params, message, callback}) => {
      if (!voiceChannelBots.has(message.member.voiceChannel.id)) {
        callback('You should play a song first')
        return
      }
      fetch(`${voiceChannelBots.get(message.member.voiceChannel.id).ip}/queue`, {
        method: 'POST',
        body: JSON.stringify({
          guild: { id: message.guild.id },
          voiceChannel: { id: message.member.voiceChannel.id },
          textChannel: { id: message.channel.id }
        }),
        headers: { 'Content-Type': 'application/json' }
      })
        .then(response => {
          return response.json()
        }, console.error)
        .then(data => {
          console.log(data)
          const queue = data.data
          var output = ''
          for (var i in queue) {
            const song = queue[i]
            if (!song.live) {
              // Song is NOT a radio station
              // Current song
              if (i == 0) output += '**Now Playing:**\n'
              else if (i == 1)
                // Next song
                output += '\n**Next:**\n'
            } else {
              // Song is a radio station
              output += (i == 0 ? '' : '\n') + '**Radio Station:**\n'
            }
            if (i != 0) output += `\`${i}:\` `
            output += `${song.title}\n`
          }
          output += `\n**${queue.length} in Queue**`
          callback(output)
        })
        .catch(console.error)
    }
  })
  .command('removesong', {
    hidden: true,
    alias: ['rs'],
    run: ({params, message, callback, lang}) => {
      const queue = queues.get(message.guild)
      const songNum = parseInt(params[0])
      if (songNum > 0 && songNum < queue.length) {
        const song = queue[songNum]
        queue.splice(songNum, 1)
        callback(`Removed **${song.name}**`)
      } else {
        callback("That's not in the queue.")
      }
    }
  })
  .command('stop', {
    description: 'Stops the current song.',
    hidden: true,
    run: ({prams, message, callback}) => {
      sendMessage(message.channel, 'Stopping', 5)
      childBotRun(
        'stop',
        {
          guild: { id: message.guild.id },
          voiceChannel: { id: message.member.voiceChannel.id }
        },
        message.member.voiceChannel
      )
        .then(data => {
          callback('Stopped')
          voiceChannelBots.delete(message.member.voiceChannel.id)
        })
        .catch(() => {
          callback("Can't stop a song I'm not playing")
        })
    }
  })
  .command('volume', {
    hidden: true,
    description: 'Change the volume.',
    alias: ['vol'],
    run: ({params, message, callback}) => {
      callback({
        title: 'How to change volume',
        image: {
          url: strings.volumeimg
        }
      })
    }
  })
