const config = require('../config.json')
const { corePlugin } = require('./core')

corePlugin.command('invite', {
  description: 'Sends out an invite link',
  run: ({params, message, callback}) => {
    const inviteLink =
      'https://discordapp.com/oauth2/authorize' +
      `?client_id=${config.clientId}` +
      '&permissions=66321471&scope=bot'
    callback(
      `**Add me to your server?**\n\nClick -> **[Invite](${inviteLink})**`
    )
  }
})
