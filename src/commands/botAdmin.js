const { client } = require('../discordClient')
const request = require('request').defaults({ encoding: null })
const { createPlugin } = require('../plugin')
const { commands } = require('../commander')

createPlugin('BotAdmin')
  .command('setusername', {
    admin: true,
    run: ({params, message, callback, lang}) => {
      client.user.setUsername(params.join(' '))
    }
  })
  .command('seticon', {
    admin: true,
    run: ({params, message, callback}) => {
      request(params[0], function(err, res, body) {
        if (!err && res.statusCode === 200) {
          var data =
            'data:' +
            res.headers['content-type'] +
            ';base64,' +
            new Buffer(body).toString('base64')
          client.user.setAvatar(data, callback())
        }
      })
    }
  })
  .command('servercount', {
    admin: true,
    run: ({params, message, callback}) => {
      callback(`${client.guilds.array().length}`)
    }
  })
  .command('usercount', {
    admin: true,
    run: ({params, message, callback}) => {
      var count = 0
      const guilds = client.guilds.array()
      for (var i in guilds) {
        const guild = guilds[i]
        count += guild.memberCount
      }
      callback(`${count} users.`)
    }
  })
  .command('serverlist', {
    admin: true,
    run: ({params, message, callback}) => {
      var output = ''
      const guilds = client.guilds.array()
      for (var i in guilds) {
        const guild = guilds[i]
        output += guild.name + ' - ' + guild.memberCount + '\n'
      }
      callback(output)
    }
  })
  .command('serverinvites', {
    admin: true,
    run: ({params, message, callback}) => {
      const guilds = client.guilds.array()
      var inviteNum = 0
      var inviteStr = ''
      for (var i in guilds) {
        const guild = guilds[i]
        try {
          guild
            .fetchInvites()
            .then(invites => {
              const invite = invites.array()[0]
              console.log(`Fetched ${invites.size} invites`)
              if (invite) inviteStr += 'https://discord.gg/' + invite.code + '\n'
              inviteNum++
              if (inviteNum >= guilds.length) {
                message.channel.send(inviteStr)
              }
            })
            .catch(console.error)
        } catch (e) {
          inviteNum++
          if (inviteNum >= guilds.length) {
            message.channel.send(inviteStr)
          }
        }
      }
    }
  })
