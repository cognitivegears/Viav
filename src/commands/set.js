const { client } = require('../discordClient')
const { commands } = require('../commander')
const config = require('../config.json')
const { createPlugin } = require('../plugin')

const {
  getConfig,
  getGuildConfig,
  setConfig,
  mergeConfig
} = require('../util/guildFunctions')

const plugin = createPlugin('Config')
  // Prints the current state of the guild config
  .command('fullconfig', {
    description: 'Guild config.',
    admin: true,
    run: ({params, message, callback}) => {
      callback(
        '```json\n' + JSON.stringify(getConfig(message.guild.id), null, 2) + '```'
      )
    }
  })
  // Prints the current state of the guild config
  .command('configjson', {
    description: 'Guild config.',
    admin: true,
    run: ({params, message, callback}) => {
      callback(
        '```json\n' + JSON.stringify(getGuildConfig(message.guild.id), null, 2) + '```'
      )
    }
  })
  // Shows server altered configs
  .command('config', {
    owner: true,
    run: ({params, message, callback}) => {
      const guildConfig = getGuildConfig(message.guild.id)
      var output = ''
      for (var i in guildConfig) {
        output += `${i}: \`${guildConfig[i]}\`\n`
      }
      callback('**Configs:**\n' + output + '')
    }
  })
  .command('botconfig', {
    description: 'Bot config.',
    admin: true,
    // Prints default configuration options
    run: ({params, message, callback}) => {
      callback('```json\n' + JSON.stringify(config, null, 2) + '```')
    }
  })

function set(setadd, params, message, callback) {
  // Check if adding to object or array
    const valInd = params.length - 1
    // Cast booleans
    if (params[valInd] === 'true') params[valInd] = true
    if (params[valInd] === 'false') params[valInd] = false
    // Apply values to output
    const output = setadd === 'set'
    ? {
      [params[0]]:
        valInd === 1
          ? params[1]
          : {
              [params[1]]: params[2]
            }
    }
    : {
      [params[0]]:
        valInd === 1
          ? [params[1]]
          : {
              [params[1]]: params[2]
            }
    }

    console.log(JSON.stringify(output, null, 2))
    // Merge output with guild config
    mergeConfig(message.guild.id, output)
    callback(`\`${params[1]}\` has been added to \`${params[0]}\``)
}


// Set and Add commands
plugin
  .command('set', {
    owner: true,
    run: ({params, message, callback}) => {
      set('set', params, message, callback)
    }
  })
  .command('add', {
    owner: true,
    run: ({params, message, callback}) => {
      set('add', params, message, callback)
    }
  })

// Converts a user to an ID, then calls commands.add to add it to the operator variable in the guild config.
plugin.command('addop', {
  owner: true,
  run: ({params, message, callback, lang}) => {
    if (message.mentions.members.array().length > 0) {
    commands.add.run(
      ['operators', message.mentions.members.array()[0].id],
      message,
      callback
    )
    } else {
      callback('Mention the person you would like to add')
    }
  }
})
