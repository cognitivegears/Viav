String.prototype.padding = function(n, c) {
  const val = this.valueOf()
  if (Math.abs(n) <= val.length) {
    return val
  }
  const m = Math.max(Math.abs(n) - this.length || 0, 0)
  const pad = Array(m + 1).join(String(c || ' ').charAt(0))
  //      var pad = String(c || ' ').charAt(0).repeat(Math.abs(n) - this.length);
  return n < 0 ? pad + val : val + pad
  //      return (n < 0) ? val + pad : pad + val;
}
