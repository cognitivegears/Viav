function isObject(item) {
  return item && typeof item === 'object' && !Array.isArray(item)
}

function isArray(item) {
  return item && Array.isArray(item)
}

function arrayUnique(array) {
  const a = array.concat()
  for (let i = 0; i < a.length; ++i) {
    for (let j = i + 1; j < a.length; ++j) {
      if (a[i] === a[j]) a.splice(j--, 1)
    }
  }

  return a
}

function mergeDeep(target, source) {
  const newObject = Object.assign({}, target)

  if (isObject(newObject) && isObject(source)) {
    for (const key in source) {
      if (isObject(source[key])) {
        // Value is an object
        if (!newObject[key]) Object.assign(newObject, { [key]: {} })
        newObject[key] = mergeDeep(newObject[key], source[key])
      } else if (isArray(source[key])) {
        // Value is an array
        if (!isArray(newObject[key])) Object.assign(newObject, { [key]: [] })
        newObject[key] = arrayUnique([...newObject[key], ...source[key]])
      } else {
        // Value is a primitive
        Object.assign(newObject, { [key]: source[key] })
      }
    }
  }
  return newObject
}
exports.mergeDeep = mergeDeep
