const { plugins } = require('./commander')
const { client } = require('./discordClient')
const { getConfig } = require('./util/guildFunctions')

exports.createPlugin = (pluginName) => {
  const plugin = {
    name: pluginName,
    commands: {},
    command: (name, command) => {
      plugin.commands[name] = command
      if (command.alias) {
        for (var i in command.alias) {
          let alias = command.alias[i]
          let aliasCommand = Object.assign({}, command)
          plugin.commands[alias] = aliasCommand
          delete aliasCommand.alias
          aliasCommand.hidden = true
        }
        delete command.alias
      }
      return plugin
    },
    clientOn: (name, event) => {
      client.on(name, (...params) => {
        const guildConfig = getConfig(event.guild(params).id)
        if (guildConfig.plugins[pluginName]) {
          event.run(params)
        }
      })
      return plugin
    }
  }
  plugins[pluginName] = plugin
  return plugin
}