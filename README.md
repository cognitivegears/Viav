Report a [Bug](https://github.com/BrandonDyer64/Viav/issues/new?template=bug_report.md&labels=bug) | Suggest a [Feature](https://github.com/BrandonDyer64/Viav/issues/new?template=feature_request.md&labels=enhancement) | Make a [Donation](https://www.patreon.com/manbabies)

[![Discord Bots](https://discordbots.org/api/widget/status/446151195338473485.svg?noavatar=true)](https://discordbots.org/bot/446151195338473485) [![Discord Bots](https://discordbots.org/api/widget/servers/446151195338473485.svg?noavatar=true)](https://discordbots.org/bot/446151195338473485)

# Viav
Pronounced Vive (_vēv_).

A Discord bot. `;help`, probably.

To **Add Viav** to your server click [here](https://discordapp.com/oauth2/authorize?client_id=446151195338473485&permissions=66321471&scope=bot). The bot needs **admin** to function properly!

## Infinite Voice Channels

**1** Voice Channel = **Infinite** Voice Channels

No more needing to make specialized channels for everything and running out of room and trying to keep everyone happy and trying to keep your sanity. Viav will lose its mind so you don't have to.

## 24/7 Music

With radio stations and infinite queues, Viav will keep you jamming all night long. The radio stations (`;stations`, `;radio [station]`) are live stations managed by 3rd parties. The infinite queue (`;play [song]`) will keep adding songs as they're played.

## Multi-channel Music (coming soon)

Play music in up to 5 voice channels at once.

## Temporary Responses

Anything that Viav says in a text channel is temporary. When you give the bot a command the command message is instantly deleted and Viav's response is deleted shortly after it's posted. No more clutter. No more _bot-spam_ channels.

# Help!

Calm down!

If you're here because you're stuck, or something's got broke real bad, head on over to our [Discord Server](https://discord.gg/EkDSE5e). We are happy to help.

# Host Viav

If you host Viav yourself please give us credit (this bot is a lot of work), and it would be super cool if you left our donation link in the `;help` command.

## Install

### Prerequisites

You must have `python`, `ffmpeg`, `make`, and `g++` installed

#### Ubuntu

```bash
sudo apt install python ffmpeg make g++
```

### Clone and Install

```bash
git clone https://github.com/BrandonDyer64/Viav.git
cd Viav
npm install
```

## Configure

```bash
cp config.example.json config.json
```

Edit the new `config.json` file to your liking.

The token and key provided in the example are fake. You'll need to get your own.

## Get API Keys

### Discord Token

Create a new app [here](https://discordapp.com/developers/applications/me).

Add a new bot user.

Copy the token to `config.json`.

Click `Generate OAuth2 URL`.

Set `bot` and `Administrator`.

Go to the generated link and add the bot to your server.

### YouTube API Key

I haven't gotten this far yet in the docs. Do some Googling or something.
